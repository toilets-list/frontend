document.addEventListener("DOMContentLoaded", ready);
var map
var toiletsList

function ready() {
    let latitude = 10.0
    let longitude = 10.0
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            setMapCenter(latitude, longitude)
        });
    }

    map = new OpenLayers.Map("map");//инициализация карты
    let mapnik = new OpenLayers.Layer.OSM();//создание слоя карты
    map.addLayer(mapnik);//добавление слоя
    let layerMarkers = new OpenLayers.Layer.Markers("Markers");//создаем новый слой маркеров
    map.addLayer(layerMarkers);//добавляем этот слой к карте
    setMapCenter(latitude, longitude)
    refreshToiletsList()
}

function setMapCenter(latitude, longitude) {
    map.setCenter(lonlat(longitude, latitude),
        14 // масштаб
    );
}

function refreshToiletsList() {
    layerMarkers = getMarkersLayer()
    if (layerMarkers == undefined) {
        return
    }

    for (let marker of layerMarkers.markers) {
        marker.destroy()
    }

    toiletsList = getToiletsList()
    for (let markerInfo of toiletsList) {
        let size = new OpenLayers.Size(32, 32);//размер картинки для маркера
        let offset = new OpenLayers.Pixel(-(size.w / 2), -size.h); //смещение картинки для маркера
        let icon = new OpenLayers.Icon('/images/pin.png', size, offset);//картинка для маркера
        let marker = new OpenLayers.Marker(lonlat(markerInfo.lon, markerInfo.lat), icon);
        layerMarkers.addMarker(marker, icon);
        markerInfo.marker = marker;

        marker.events.register('click', map, function (markerInfo) {    
            return function(e) {
                console.log(e);
                console.log(markerInfo);
            }
        }(markerInfo));
    }
}

function lonlat(longitude, latitude) {
    return new OpenLayers.LonLat(longitude, latitude)
    .transform(
      new OpenLayers.Projection("EPSG:4326"), // переобразование в WGS 1984
      new OpenLayers.Projection("EPSG:900913") // переобразование проекции
    )
}

function getMarkersLayer() {
    for (let layer of map.layers) {
        if (layer.name == "Markers") {
            return layer
        }
    }
}

function getToiletsList() {
    return [
        {
            id: 1,
            lon: 37.493196,
            lat: 55.751774,
            marker: undefined,
        },
    ];
}
